## 1.购买安装ECS
- [购买地址](https://ecs-buy.aliyun.com/wizard/#/prepay/cn-hangzhou)
- 选择按需付费，配配置选择共享标准型s6 4vCPU 8GiB
- 操作系统选择 CentoOS 8.4 64位
- 分配公网IPv4地址

![](https://static.zhufengpeixun.com/1buyecs_1637119194761.png)
![](https://static.zhufengpeixun.com/2buyecs_1637129255012.png)
![](https://static.zhufengpeixun.com/3buyecs_1637119216339.png)


## 1.安装docker
```js
yum install -y yum-utils   device-mapper-persistent-data   lvm2
yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io -y
systemctl start docker
docker version
docker info
```

## 2.安装docker-compose
```js
sudo curl -L "https://static.zhufengpeixun.com/dockercomposeLinuxx8664_1637129076349" -o /usr/local/bin/docker-compose
# 添加可执行权限
sudo chmod +x /usr/local/bin/docker-compose
# 查看版本信息 
docker-compose -version
```

## 3.安装git并下载仓库 
```js
yum install git -y
git clone https://gitee.com/zhufengpeixun/onpremise
```


## 4.安装sentry
```js
cd onpremise
./install.sh
```

### 5. 启动
- 安装过程中会让我们创建 `sentry` 后台管理员账号，可以先跳过，后面再创建
- 在 `onpremise` 文件中中启动服务

```bash
docker-compose up -d
```

### 6.开放端口
![](https://static.zhufengpeixun.com/security_1637131020208.png)
![](https://static.zhufengpeixun.com/security2_1637131034345.png)
![](https://static.zhufengpeixun.com/security3_1637131043477.png)

### 7.访问
- 浏览器访问 http://IP:9000 ，进入 `sentry` 管理界面

![](https://static.zhufengpeixun.com/sentrylogin_1637131160321.png)
